import { get, post } from 'axios'

import { MAIN_API } from 'constants/index'

export const START_FETCHING = 'START_FETCHING_INFORMATION'
export const ERROR_RESULT = 'ERROR_RESULT_INFORMATION'
export const SUCCESS_RESULT = 'SUCCESS_RESULT_INFORMATION'
export const LOGIN = 'LOGIN_INFORMATION'

const startFetchingAction = { type: START_FETCHING }
const failed = { type: ERROR_RESULT }
const success = { type: SUCCESS_RESULT }
const login = { type: LOGIN }

export function searchResults(query, token) {
  return async (dispatch) => {
    dispatch(startFetchingAction)

    try {
      const response = await get(
        `${MAIN_API}/searchEngine`,
        { params: { q: query }, headers: { Authorization: `JWT ${token}` }}
      )

      if(!response.data || !response.data.data) return dispatch({ ...failed, error: 'Unknown' })

      dispatch({ ...success, information: response.data.data })
    } catch(error) {
      dispatch({ ...failed, error })
    }
  }
}

export function getToken() {
  return async (dispatch) => {
    try {
      const response = await post(`${MAIN_API}/api/v1/auth`, { username: 'admin', password: 'admin' })

      console.log('RESPONSE', response)
      if(!response.data || !response.data.access_token) return dispatch({ ...failed, error: 'Unknown' })

      dispatch({ ...login, token: response.data.access_token })
    } catch(error) {
      dispatch({ ...failed, error })
    }
  }
}
