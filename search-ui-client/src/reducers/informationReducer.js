import mockResponse from 'mock/response.json'
import { START_FETCHING, ERROR_RESULT, SUCCESS_RESULT, LOGIN } from 'actions/informationAction'

const INITIAL_STATE = {
  data: mockResponse.data,
  isFetching: false,
  errors: [],
  token: null,
}
const handlers = {
  [START_FETCHING]: (state) => ({ ...state, isFetching: true }),
  [ERROR_RESULT]: (state, { error }) => ({ ...state, isFetching: false, errors:  [error]}),
  [SUCCESS_RESULT]: (state, { information }) => ({ ...state, isFetching: false, data: information, errors: [] }),
  [LOGIN]: (state, { token }) => ({ ...state, isFetching: false, token, errors: [] }),
}

export default function informationReducer(state = INITIAL_STATE, { type, ...payload }) {
  return handlers[type] ? handlers[type](state, payload) : state
}
