import React, { PureComponent } from 'react'

class SearchBar extends PureComponent {
  constructor(props) {
    super(props)

    this.state = { input: '' }
  }

  render() {
    return (
      <form className="form-inline" onSubmit={this._handleSubmit}>
        <div className="form-group">
          <label htmlFor="exampleInputName2">Search:</label>
          <input
            type="text"
            className="form-control"
            value={this.state.input}
            onChange={this._handleChange}
          />
        </div>
        <button type="submit" className="btn btn-default">Submit</button>
      </form>
    )
  }

  _handleChange = (event) => this.setState({ input: event.target.value })

  _handleSubmit = (event) => {
    event.preventDefault()

    this.props.onSubmit(this.state.input)
  }
}

export default SearchBar
