import React, { Component } from 'react'
import { connect } from 'react-redux'

import { SearchBar } from 'components'
import { searchResults } from 'actions/informationAction'

@connect(
  ({ informationReducer }) => ({ token: informationReducer.token }),
  { searchResults }
)
class Header extends Component {
  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {
    return (
      <header>
        <h1>Search Engine</h1>
        <SearchBar onSubmit={this._handleSubmit} />
      </header>
    )
  }

  _handleSubmit = (input) => {
    this.props.searchResults(input, this.props.token)
  }
}

export default Header
