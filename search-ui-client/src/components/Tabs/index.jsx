import React, { PureComponent } from 'react'
import classnames from 'classnames'

export function Tab({ id, children, active }): ReactElement {
  return (
    <div id={id} className={classnames('tab-pane fade', { 'active in': active })}>
      {children}
    </div>
  )
}

class Tabs extends PureComponent {
  render() {
    return (
      <section>
        <ul className="nav nav-tabs">
          {this.props.children.map(({ props: { id, title, active, onTabClick }}) => (
            <li
              role="presentation"
              className={classnames({ active })}
              onClick={onTabClick(id)}
            >
              <a data-toggle="tab" href={`#${id}`}>{title}</a>
            </li>
          ))}
        </ul>
        <div className="tab-content">
          {this.props.children}
        </div>
      </section>
    )
  }
}

export default Tabs
