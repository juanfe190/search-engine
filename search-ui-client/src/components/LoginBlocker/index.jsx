import React from 'react'

import './styles.css'

function LoginBlocker({ show, onLogin }): ReactElement {
  if(!show) return null

  return (
    <div id="login-blocker">
      <button type="button" onClick={onLogin} className="btn btn-default btn-lg">Login</button>
    </div>
  )
}

export default LoginBlocker
