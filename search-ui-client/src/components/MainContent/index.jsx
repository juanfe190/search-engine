import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import { Tabs, Tab, LoginBlocker } from 'components'
import { getToken } from 'actions/informationAction'

import './styles.css'

const EMPTY_ARRAY = []

@connect(
  ({ informationReducer }) => ({
    information: informationReducer.data.information || EMPTY_ARRAY,
    images: informationReducer.data.images || EMPTY_ARRAY,
    videos: informationReducer.data.videos || EMPTY_ARRAY,
    errors: informationReducer.errors,
  }),
  { getToken }
)
class MainContent extends PureComponent {
  constructor(props) {
    super(props)

    this.state = { activeTab: 'item1' }
  }

  render() {
    const { activeTab } = this.state
    const { information, images, videos, errors } = this.props

    return (
      <main>
        <LoginBlocker show={Boolean(errors.length)} onLogin={this._handleLogin} />
        <Tabs>
          <Tab onTabClick={this._handleTabClick} id="item1" title="Web" active={activeTab === 'item1'}>
            <section id="web-content">
              {information.map(({ formattedUrl, title, snippet }) => (
                <div className="web-link-container">
                  <a href={formattedUrl}>{title}</a>
                  <p>{snippet}</p>
                </div>
              ))}
            </section>
          </Tab>
          <Tab onTabClick={this._handleTabClick} id="item2" title="Images" active={activeTab === 'item2'}>
            <section id="images">
              {images.map(({ link }) => (
                <div className="image-container">
                  <img src={link} alt=""/>
                </div>
              ))}
            </section>
          </Tab>
          <Tab onTabClick={this._handleTabClick} id="item3" title="Videos" active={activeTab === 'item3'}>
            <section id="videos">
              {videos.map(({ snippet: { title }, id: { videoId }}, key) => (
                <div className="video-container">
                  <iframe
                    title={key}
                    width="560"
                    height="315"
                    src={`https://www.youtube.com/embed/${videoId}`}
                    frameborder="0"
                    allowfullscreen
                  />
                </div>
              ))}
            </section>
          </Tab>
        </Tabs>
      </main>
    )
  }

  _handleTabClick = (activeTab) => (() => this.setState({ activeTab }))

  _handleLogin = () => this.props.getToken()
}

export default MainContent
