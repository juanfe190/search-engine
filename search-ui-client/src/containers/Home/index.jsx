import React, { PureComponent } from 'react'

import { Header, MainContent } from 'components'

import './styles.css'

class Home extends PureComponent {
  render() {
    return (
      <div id="main-wrapper">
        <Header />
        <MainContent />
      </div>
    )
  }
}

export default Home
