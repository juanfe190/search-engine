import { combineReducers, createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'

import * as reducers from '../reducers'
console.log('REDUCERS', reducers)

export default function configureStore() {
  return createStore(
    combineReducers(reducers),
    applyMiddleware(thunkMiddleware)
  )
}
