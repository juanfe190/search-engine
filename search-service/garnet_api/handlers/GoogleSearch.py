from os import environ
import requests

GOOGLE_SEARCH_API = 'https://www.googleapis.com/customsearch/v1?cx=017772401011533031801%3Ad467salpb6g'
GOOGLE_YOUTUBE_API = 'https://www.googleapis.com/youtube/v3/search'
CREDENTIALS = { 'key': environ.get('GOOGLE_KEY') }

class GoogleSearch:
  def __init__(self, query):
    self.query = query

  def getImages(self):
    payload = _buildPayload('image', self.query)
    response = requests.get(GOOGLE_SEARCH_API, params=payload).json()
    images = response.get('items', [])

    return images

  def getInformation(self):
    payload = _buildPayload('info', self.query)
    response = requests.get(GOOGLE_SEARCH_API, params=payload).json()

    return response.get('items', [])

  def getVideos(self):
    payload = CREDENTIALS.copy()
    payload['q'] = self.query
    payload['part'] = 'snippet'
    response = requests.get(GOOGLE_YOUTUBE_API, params=payload).json()

    return response.get('items', [])

def _buildPayload(target, query):
  payload = CREDENTIALS.copy()
  payload['q'] = query

  if(target == 'image'):
    payload['searchType'] = 'image'
    payload['imgSize'] = 'medium'

  return payload
