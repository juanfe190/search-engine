from flask import request
from time import time

from garnet_api.models.log import Log
from garnet_api.handlers.Slack import Slack

def logger(func):
  def decorated_function():
    query=request.args.get('q')
    client_ip=request.environ['REMOTE_ADDR']
    timestamp=int(time())
    slack = Slack()

    Log(query=query, client_ip=client_ip, timestamp=timestamp).save()
    slack.sendMessage(query, client_ip, timestamp)

    return func()
  return decorated_function
