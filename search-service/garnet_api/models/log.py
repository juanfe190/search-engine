from mongoengine import *

class Log(Document):
  query = StringField(required=True)
  client_ip = StringField(required=True)
  timestamp = IntField(required=True)
