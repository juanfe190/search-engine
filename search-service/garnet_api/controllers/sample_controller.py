from garnet_api import app
from flask import jsonify
from os import environ

from garnet_api.handlers.Slack import Slack


# --------------------------------------------------------------------------
# ROOT RESOURCE OF THE API
# --------------------------------------------------------------------------
#
@app.route('/', methods=['GET'])
def get_api_root():
  return environ.get('GOOGLE_KEY')
