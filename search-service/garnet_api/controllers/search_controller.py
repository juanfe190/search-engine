from flask import jsonify, request
from flask_jwt import jwt_required

from garnet_api import app
from garnet_api.handlers.GoogleSearch import GoogleSearch
from garnet_api.extensions.logger import logger

@app.route('/searchEngine', methods=['GET'])
@jwt_required()
@logger
def query():
  # return jsonify({})

  query = request.args.get('q')
  googleSearch = GoogleSearch(query)
  images = googleSearch.getImages()
  information = googleSearch.getInformation()
  videos = googleSearch.getVideos()

  return jsonify({
    'data': {
      'images': images,
      'information': information,
      'videos': videos,
    }
  })
